package rpc

import (
	"context"
	"gitee.com/MinKo-U/mcenter/apps/instance"
	"gitee.com/MinKo-U/mcenter/client/rpc/lifecycle"
)

func (c *ClientSet) Registry(ctx context.Context, req *instance.RegistryRequest) (lifecycle.Lifecycler, error) {
	ins, err := c.Instance().RegistryInstance(ctx, req)
	if err != nil {
		return nil, err
	}

	lc := lifecycle.NewLifecycler(c.Instance(), ins)
	return lc, nil
}
