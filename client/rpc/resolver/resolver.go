package resolver

import (
	"context"
	"fmt"
	"gitee.com/MinKo-U/mcenter/apps/instance"
	"gitee.com/MinKo-U/mcenter/client/rpc"
	"github.com/infraboard/mcube/logger"
	"github.com/infraboard/mcube/logger/zap"
	"google.golang.org/grpc/resolver"
	"time"
)

const (
	Scheme = "mcenter"
)

var (
	_ resolver.Builder  = (*McenterResolverBuild)(nil)
	_ resolver.Resolver = (*mcenterResolver)(nil)
)

type McenterResolverBuild struct {
}

func (m *McenterResolverBuild) Build(target resolver.Target,
	cc resolver.ClientConn,
	opts resolver.BuildOptions) (
	resolver.Resolver, error) {
	r := &mcenterResolver{

		mcenter:         rpc.C().Instance(),
		target:          target,
		cc:              cc,
		queryTimeSecond: 3 * time.Second,
		log:             zap.L().Named("mcenter resolver"),
	}
	r.ResolveNow(resolver.ResolveNowOptions{})
	M.add(r)
	return r, nil
}

func (m *McenterResolverBuild) Scheme() string {
	return Scheme
}

type mcenterResolver struct {
	mcenter         instance.ServiceClient
	target          resolver.Target
	cc              resolver.ClientConn
	queryTimeSecond time.Duration
	log             logger.Logger
}

func (m *mcenterResolver) Close() {}

func (m *mcenterResolver) ResolveNow(options resolver.ResolveNowOptions) {
	//查出对应实例
	addrs, err := m.search()
	if err != nil {
		m.log.Errorf("search target %s error, %s", m.target.URL.String(), err)
	}
	//把端点地址更新给client
	m.cc.UpdateState(resolver.State{Addresses: addrs})
}

func (m *mcenterResolver) search() ([]resolver.Address, error) {
	req := m.buildreq()
	if req.ServiceName == "" {
		return nil, fmt.Errorf("application name required")
	}
	ctx, cancel := context.WithTimeout(context.Background(), m.queryTimeSecond)
	defer cancel()
	set, err := m.mcenter.Search(ctx, req)
	if err != nil {
		m.log.Errorf("search target %s error, %s", m.target, err)
		return nil, err
	}
	items := set.GetGroupInstance(req.Group)
	if len(items) == 0 {
		items = set.GetOldGroup()
	}
	addrs := make([]resolver.Address, len(items))
	for i, s := range items {
		addrs[i] = resolver.Address{Addr: s.RegistryInfo.Address}
	}
	m.log.Infof("search application address: %s", addrs)

	return addrs, nil
}

func (m *mcenterResolver) buildreq() *instance.SearchRequest {
	searchReq := instance.NewSearchRequest()
	searchReq.ServiceName = m.target.URL.Host
	qs := m.target.URL.Query()
	searchReq.Page.PageSize = 500
	searchReq.Region = qs.Get("region")
	searchReq.Environment = qs.Get("environment")
	return searchReq
}

func init() {
	// Register the mcenter ResolverBuilder. This is usually done in a package's
	// init() function.
	resolver.Register(&McenterResolverBuild{})
}
