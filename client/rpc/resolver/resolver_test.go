package resolver_test

import (
	"context"
	"fmt"
	"gitee.com/MinKo-U/mcenter/client/rpc"
	"gitee.com/MinKo-U/mcenter/client/rpc/auth"
	"gitee.com/MinKo-U/mcenter/client/rpc/resolver"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
	"testing"
	"time"
)

const (
	ClientId     = "f7NtcYzyycmoXeK09nsmrwUL"
	ClientSecret = "xSVVlWuZTINeL6aIRiQFFohD9G6H8gmX"
)

func TestResolver(t *testing.T) {
	con := rpc.NewDefaultConfig()
	con.ClientID = ClientId
	con.ClientSecret = ClientSecret
	rpc.LoadClientFromConfig(con)
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*10)
	defer cancel()

	conn, err := grpc.DialContext(ctx, fmt.Sprintf("%s://%s", resolver.Scheme, "keyauth"),
		grpc.WithPerRPCCredentials(auth.NewAuthentication(ClientId, ClientSecret)),
		grpc.WithTransportCredentials(insecure.NewCredentials()),
		grpc.WithBlock())
	if err != nil {
		t.Fatalf("did not connect:%v", err)
	}
	defer conn.Close()
}
