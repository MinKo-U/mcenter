package resolver

import (
	"google.golang.org/grpc/resolver"
	"sync"
)

// 全局Manager对象
var M = manager{}

type manager struct {
	resolvers []*mcenterResolver
	lock      sync.Mutex
}

func (m *manager) add(ins *mcenterResolver) {
	m.lock.Lock()
	defer m.lock.Unlock()
	if m.resolvers == nil {
		m.resolvers = []*mcenterResolver{}
	}
	m.resolvers = append(m.resolvers, ins)
}

func (m *manager) Update() {
	for i := range m.resolvers {
		m.resolvers[i].ResolveNow(resolver.ResolveNowOptions{})
	}
}
