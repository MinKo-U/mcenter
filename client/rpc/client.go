package rpc

import (
	"gitee.com/MinKo-U/mcenter/apps/instance"
	"gitee.com/MinKo-U/mcenter/apps/service"
	"github.com/infraboard/mcube/logger"
	"github.com/infraboard/mcube/logger/zap"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

var (
	client *ClientSet
)

// C Global
func C() *ClientSet {
	if client == nil {
		panic("mcenter client config not load")
	}
	return client
}

func LoadClientFromConfig(conf *Config) error {
	c, err := NewClient(conf)
	if err != nil {
		return err
	}
	client = c
	return nil
}

// NewClient todo
func NewClient(conf *Config) (*ClientSet, error) {
	zap.DevelopmentSetup()
	log := zap.L()

	conn, err := grpc.Dial(
		conf.Address,
		grpc.WithTransportCredentials(insecure.NewCredentials()),
		grpc.WithPerRPCCredentials(conf.Credentials()),
	)
	if err != nil {
		return nil, err
	}

	return &ClientSet{
		conn: conn,
		log:  log,
	}, nil
}

// Client 客户端
type ClientSet struct {
	conn *grpc.ClientConn
	log  logger.Logger
}

// instance服务的SDK
func (c *ClientSet) Instance() instance.ServiceClient {
	return instance.NewServiceClient(c.conn)
}
func (c *ClientSet) Service() service.MetaServiceClient {
	return service.NewMetaServiceClient(c.conn)
}
