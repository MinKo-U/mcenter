package auth

import (
	"context"
	"fmt"
	"gitee.com/MinKo-U/mcenter/apps/service"
	"github.com/infraboard/mcube/logger"
	"github.com/infraboard/mcube/logger/zap"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/metadata"
	"google.golang.org/grpc/status"
)

const (
	ClientHeaderKey = "client-id"
	ClientSecretKey = "client-secret"
)

func GrpcAuthUnaryServerInterceptor(svr service.MetaServiceClient) grpc.UnaryServerInterceptor {
	return newGrpcAuther(svr).Auth
}

func newGrpcAuther(svr service.MetaServiceClient) *grpcAuther {
	return &grpcAuther{
		log:     zap.L().Named("Grpc Auther"),
		service: svr,
	}
}

type grpcAuther struct {
	log     logger.Logger
	service service.MetaServiceClient
}

func (g *grpcAuther) Auth(ctx context.Context, req interface{},
	info *grpc.UnaryServerInfo,
	handler grpc.UnaryHandler) (resp interface{}, err error) {
	md, ok := metadata.FromIncomingContext(ctx)
	if !ok {
		return nil, fmt.Errorf("ctx is not an grpc incoming context")
	}
	clientId, clientSecret := g.GetClientCredentialsFromMeta(md)
	if err = g.ValidateServiceCredential(clientId, clientSecret); err != nil {
		return nil, err
	}

	return handler(ctx, req)
}

func (g *grpcAuther) GetClientCredentialsFromMeta(md metadata.MD) (clientId, clientSecrt string) {
	cids, sids := md.Get(ClientHeaderKey), md.Get(ClientSecretKey)
	if len(cids) > 0 {
		clientId = cids[0]
	}
	if len(sids) > 0 {
		clientSecrt = sids[0]
	}
	return
}

func (g *grpcAuther) ValidateServiceCredential(clientId, clientSecret string) error {
	if clientId == "" || clientSecret == "" {
		return status.Errorf(codes.Unauthenticated, "client_id or client_secret is \"\"")
	}
	vsReq := service.NewValidateCredentialRequest(clientId, clientSecret)
	app, err := g.service.ValidateCredential(context.Background(), vsReq)
	if err != nil {
		return status.Errorf(codes.Unauthenticated, "service auth error, %s", err)
	}
	fmt.Println(app)
	return nil

}
