package impl

import (
	"context"
	"gitee.com/MinKo-U/mcenter/apps/instance"
	"gitee.com/MinKo-U/mcenter/apps/service"
	"gitee.com/MinKo-U/mcenter/client/rpc/auth"
	"github.com/infraboard/mcube/exception"
	"io"
)

func (i *impl) RegistryInstance(ctx context.Context, req *instance.RegistryRequest) (*instance.Instance, error) {
	clientId := auth.GetClientId(ctx)
	DescSvc, err := i.app.DescribeService(ctx, service.NewDescribeServiceClientIdRequest(clientId))
	if err != nil {
		return nil, err
	}
	ins, err := instance.NewInstance(req, DescSvc)
	if err != nil {
		return nil, exception.NewBadRequest("validate create instance error, %s", err)
	}
	if err := i.upsert(ctx, ins); err != nil {
		return nil, err
	}
	return ins, nil
}

// 实例上报心跳
func (i *impl) Heartbeat(ins instance.Service_HeartbeatServer) error {
	for {
		req, err := ins.Recv()
		if err != nil {
			if err == io.EOF {
				return nil
			}
			i.log.Warnf("receive heartbeat error, %s", err)
			return nil
		}
		i.log.Debugf("instance %s", req.InstanceId)
		resp := instance.NewHeartbeatResponse()
		err = ins.Send(resp)
		if err != nil {
			return err
		}
	}

}

// 实例注销
func (i *impl) UnRegistry(ctx context.Context, req *instance.UnregistryRequest) (*instance.Instance, error) {
	ins, err := i.DescribeInstance(ctx, instance.NewDescribeInstanceRequest(req.InstanceId))
	if err != nil {
		return nil, err
	}
	if err := i.delete(ctx, ins); err != nil {
		return nil, err
	}
	return ins, nil
}

// 实例搜索, 用于客户端做服务发现
func (i *impl) Search(ctx context.Context, req *instance.SearchRequest) (*instance.InstanceSet, error) {
	ins := newSearchRequest(req)
	set, err := i.search(ctx, ins)
	if err != nil {
		return nil, err
	}

	if ins.ServiceName != "" {
		i.log.Debugf("search %s, address: %s", req.ServiceName, set.Items)
	}

	return set, nil
}

// 查询实例详情
func (i *impl) DescribeInstance(ctx context.Context, req *instance.DescribeInstanceRequest) (*instance.Instance, error) {
	return i.get(ctx, req.Id)
}
