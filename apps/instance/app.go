package instance

import (
	"fmt"
	"gitee.com/MinKo-U/mcenter/apps/service"
	"github.com/go-playground/validator/v10"
	"github.com/infraboard/mcenter/common/tools"

	"github.com/infraboard/mcube/http/request"
	"sort"
	"time"
)

const (
	AppName = "instance"
)

const (
	DefaultRegion      = "default"
	DefaultEnvironment = "default"
	DefaultGroup       = "default"
)

var (
	validate = validator.New()
)

func (r *RegistryRequest) Validate() error {
	return validate.Struct(r)
}
func NewInstance(req *RegistryRequest, app *service.Service) (*Instance, error) {
	if err := req.Validate(); err != nil {
		return nil, err
	}
	if req.Name == "" {
		req.Name = "ins" + tools.MakeBearer(8)
	}
	ins := &Instance{
		Domain:       app.Spec.Domain,
		Namespace:    app.Spec.Namespace,
		ServiceName:  app.Spec.Name,
		RegistryInfo: req,
		Status:       NewDefaultStatus(),
		Config:       NewDefaultConfig(),
	}
	return ins, nil
}

func NewDefaultStatus() *Status {
	return &Status{

		Online: time.Now().UnixMilli(),
	}
}
func NewDefaultConfig() *Config {
	return &Config{
		Heartbeat: &HeartbeatConfig{},
	}
}
func (i *Instance) FullName() string {
	return fmt.Sprintf("%s.%s.%s-%s", i.Domain, i.Namespace, i.ServiceName,
		i.RegistryInfo.InstanceFullName())
}
func (r *RegistryRequest) InstanceFullName() string {
	return fmt.Sprintf("%s.%s.%s.%s.%s", r.Region, r.Environment, r.Group, r.Name, r.Protocal)
}

func NewHeartbeatResponse() *HeartbeatResponse {
	return &HeartbeatResponse{}
}

func NewHeartbeatRequest(id string) *HeartbeatRequest {
	return &HeartbeatRequest{
		InstanceId: id,
	}
}

func NewInstanceSet() *InstanceSet {
	return &InstanceSet{}
}

func (s *InstanceSet) Add(ins *Instance) {
	s.Items = append(s.Items, ins)
}

func NewDescribeInstanceRequest(id string) *DescribeInstanceRequest {
	return &DescribeInstanceRequest{Id: id}
}

func NewRegistryRequest() *RegistryRequest {
	return &RegistryRequest{
		Region:      DefaultRegion,
		Environment: DefaultEnvironment,
		Group:       DefaultGroup,
		Tags:        map[string]string{},
		Build:       &Build{},
		Provider:    Provider_SDK,
		Protocal:    Protocal_GRPC,
	}
}
func NewSearchRequest() *SearchRequest {
	return &SearchRequest{
		Page: request.NewDefaultPageRequest(),
		Tags: map[string]string{},
	}
}
func (s *InstanceSet) GetGroupInstance(group string) (items []*Instance) {
	for i := range s.Items {
		if s.Items[i].RegistryInfo.Group == group {
			items = append(items, s.Items[i])
		}
	}
	return
}
func (s *InstanceSet) Len() int {
	return len(s.Items)
}

func (s *InstanceSet) Less(i, j int) bool {
	return s.Items[i].Status.Online < s.Items[j].Status.Online
}

func (s *InstanceSet) Swap(i, j int) {
	s.Items[i], s.Items[j] = s.Items[j], s.Items[i]
}
func (s *InstanceSet) GetOldGroup() (items []*Instance) {
	sort.Sort(s)
	if s.Len() > 0 {
		og := s.Items[0].RegistryInfo.Group
		return s.GetGroupInstance(og)
	}

	return nil
}
func NewUnregistryRequest(instanceId string) *UnregistryRequest {
	return &UnregistryRequest{
		InstanceId: instanceId,
	}
}
func NewDefaultInstance() *Instance {
	req := NewRegistryRequest()
	app := service.NewDefaultService()
	return &Instance{
		Domain:       app.Spec.Domain,
		Namespace:    app.Spec.Namespace,
		ServiceName:  app.Spec.Name,
		RegistryInfo: req,
		Status:       NewDefaultStatus(),
		Config:       NewDefaultConfig(),
	}
}
