package impl

import (
	"gitee.com/MinKo-U/mcenter/apps/service"
	"gitee.com/MinKo-U/mcenter/conf"
	"go.mongodb.org/mongo-driver/mongo"

	"github.com/infraboard/mcube/app"
	"github.com/infraboard/mcube/logger"
	"github.com/infraboard/mcube/logger/zap"
	"google.golang.org/grpc"
)

var (
	// Service 服务实例
	svr                           = &impl{}
	_   service.MetaServiceServer = (*impl)(nil)
)

type impl struct {
	col *mongo.Collection
	log logger.Logger
	service.UnimplementedMetaServiceServer
}

func (i *impl) Config() error {
	db, err := conf.C().Mongo.GetDB()
	if err != nil {
		return err
	}
	i.col = db.Collection(i.Name())

	i.log = zap.L().Named(i.Name())
	return nil
}

func (i *impl) Name() string {
	return service.AppName
}

func (i *impl) Registry(server *grpc.Server) {
	service.RegisterMetaServiceServer(server, svr)
}

func init() {
	app.RegistryGrpcApp(svr)
}
