package all

import (
	// 注册所有HTTP服务模块, 暴露给框架HTTP服务器加载
	_ "gitee.com/MinKo-U/mcenter/apps/book/api"
	_ "gitee.com/MinKo-U/mcenter/apps/instance/api"
	_ "gitee.com/MinKo-U/mcenter/apps/service/api"
)
